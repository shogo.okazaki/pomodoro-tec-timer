package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	l "github.com/aws/aws-sdk-go/service/lambda"

	s "jp.co.linkode/PomodoroTecTimer/receiveInteraction/slack"
	"jp.co.linkode/PomodoroTecTimer/shared/models"
)

// HandleRequest :
func HandleRequest(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	body := request.Body
	block, err := s.ExtractBlock(body)
	if err != nil {
		return events.APIGatewayProxyResponse{
			Body:       request.Body,
			StatusCode: http.StatusInternalServerError,
		}, nil
	}

	value := block.Value

	log.Printf("value: %s", value)

	svc := l.New(session.New())
	var functionName string
	var timerSetting models.TimerSetting
	if value == os.Getenv("WORK_END_KEYWORD") {
		functionName = os.Getenv("ARN_END_WORK")
		timerSetting = models.TimerSetting{}
	} else {
		functionName = os.Getenv("ARN_COUNT_REST_TIME")
		tmp, _ := strconv.ParseInt(value, 10, 64)
		valueInt := int64(time.Duration(tmp) * time.Minute / time.Second)
		timerSetting = models.TimerSetting{
			Time: valueInt,
		}
	}
	jsonBytes, _ := json.Marshal(timerSetting)

	input := &l.InvokeInput{
		FunctionName:   aws.String(functionName),
		Payload:        jsonBytes,
		InvocationType: aws.String("Event"),
	}
	resp, _ := svc.Invoke(input)
	log.Printf("Invoke Result: %s", resp.Payload)

	return events.APIGatewayProxyResponse{
		Body: "OK",
		Headers: map[string]string{
			"Content-Type": "text"},
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}

package slack

import (
	"encoding/json"
	"log"
	"net/url"
	"os"

	"github.com/nlopes/slack"
)

func ExtractBlock(body string) (*slack.BlockAction, error) {
	jsonStr, err := url.QueryUnescape(string(body)[8:])
	if err != nil {
		log.Printf("[ERROR]Failed to unespace request body: %s", err)
		return nil, err
	}
	log.Printf("Request body: %s", jsonStr)

	var message slack.InteractionCallback
	if err := json.Unmarshal([]byte(jsonStr), &message); err != nil {
		log.Printf("[ERROR]Failed to decode json message from slack: %s", jsonStr)
		return nil, err
	}

	if message.Token != os.Getenv("SLACK_VERIFICATION_TOKEN") {
		log.Printf("[ERROR]Invalid token: %s", message.Token)
		return nil, err
	}

	return message.ActionCallback.BlockActions[0], nil
}

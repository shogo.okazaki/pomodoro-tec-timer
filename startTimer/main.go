package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/nlopes/slack"
	"jp.co.linkode/PomodoroTecTimer/shared/models"
	// "github.com/aws/aws-sdk-go/aws"
	// "github.com/aws/aws-sdk-go/aws/session"
	// "github.com/aws/aws-sdk-go/service/dynamodb"
)

// HandleRequest :
func HandleRequest(event models.WorkDetail) error {
	jst, _ := time.LoadLocation("Asia/Tokyo")
	date := time.Now().In(jst).Format("2006-01-02 15:04:05")
	log.Printf("Work: %s", event.Content)
	log.Printf("Date: %s", date)
	message := fmt.Sprintf("Start Pomodoro : %s", date)
	api := slack.New(
		os.Getenv("SLACK_ACCESS_TOKEN"),
	)
	api.SendMessage(os.Getenv("SLACK_POST_CHANNEL_NAME"), slack.MsgOptionText(message, false))

	timeInt, _ := strconv.Atoi(os.Getenv("WORK_PERIOD"))
	time := int64(time.Duration(timeInt) * time.Minute / time.Second)

	timerSetting := models.TimerSetting{
		Time: time,
	}
	timerSettingJson, _ := json.Marshal(timerSetting)
	timerSettingString := string(timerSettingJson)

	stateMachineArn := os.Getenv("ARN_WORKPERIOD_STATE_MACHINE")
	input := &sfn.StartExecutionInput{
		Input:           &timerSettingString,
		StateMachineArn: &stateMachineArn,
	}
	svc := sfn.New(session.New())
	resp, err := svc.StartExecution(input)
	if err != nil {
		log.Printf("[ERROR]State Machine failed to run. : %s", err)
	}
	log.Printf("Responce: %s", resp)

	return nil

	// sess, err := session.NewSession()
	// if err != nil {
	// 	panic(err)
	// }

	// svc := dynamodb.New(sess)

	// putParams := &dynamodb.PutItemInput{
	// 	TableName: aws.String("Pomodoro-Record"),
	// 	Item: map[string]*dynamodb.AttributeValue{
	// 		"id": {
	// 			S: aws.String("2"),
	// 		},
	// 		"name": {
	// 			S: aws.String("hoge"),
	// 		},
	// 	},
	// }

	// putItem, putErr := svc.PutItem(putParams)
	// if putErr != nil {
	// 	panic(putErr)
	// }
	// fmt.Println(putItem)
}

func main() {
	lambda.Start(HandleRequest)
}

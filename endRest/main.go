package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/nlopes/slack"
)

func HandleRequest() error {
	jst, _ := time.LoadLocation("Asia/Tokyo")
	date := time.Now().In(jst).Format("2006-01-02 15:04:05")
	message := fmt.Sprintf("End rest : %s \n Mentions the application and declare the start of work.", date)
	api := slack.New(
		os.Getenv("SLACK_ACCESS_TOKEN"),
	)
	_, _, _, err := api.SendMessage(os.Getenv("SLACK_POST_CHANNEL_NAME"), slack.MsgOptionText(message, false))
	if err != nil {
		log.Printf("[ERROR]Failed to post message. : %s", err)
		return err
	}
	log.Print("Success to post message.")
	return nil
}

func main() {
	lambda.Start(HandleRequest)
}

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sfn"
	"github.com/nlopes/slack"

	"jp.co.linkode/PomodoroTecTimer/shared/models"
)

func HandleRequest(event models.TimerSetting) error {
	// Slack に休憩開始の旨のメッセージを投稿
	jst, _ := time.LoadLocation("Asia/Tokyo")
	date := time.Now().In(jst).Format("2006-01-02 15:04:05")
	message := fmt.Sprintf("Start Rest : %s", date)
	api := slack.New(
		os.Getenv("SLACK_ACCESS_TOKEN"),
	)
	api.SendMessage(os.Getenv("SLACK_POST_CHANNEL_NAME"), slack.MsgOptionText(message, false))

	// StepFunctions のステートマシンを起動
	svc := sfn.New(session.New())
	jsonValue, _ := json.Marshal(event)
	value := string(jsonValue)
	stateMachineArn := os.Getenv("ARN_RESTTIME_STATE_MACHINE")
	input := &sfn.StartExecutionInput{
		Input:           &value,
		StateMachineArn: &stateMachineArn,
	}
	resp, err := svc.StartExecution(input)
	if err != nil {
		log.Printf("[ERROR]State Machine failed to run. : %s", err)
		return err
	}
	log.Printf("Responce: %s", resp)

	return nil
}

func main() {
	lambda.Start(HandleRequest)
}

package models

type TimerSetting struct {
	Time int64 `json:"time"`
}

type WorkDetail struct {
	Content string `json:content`
}

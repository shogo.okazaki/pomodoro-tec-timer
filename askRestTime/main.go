package main

import (
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/nlopes/slack"
)

func HandleRequest() error {

	api := slack.New(
		os.Getenv("SLACK_ACCESS_TOKEN"),
	)

	_, _, _, err := api.SendMessage(os.Getenv("SLACK_POST_CHANNEL_NAME"), createBlockKit())
	if err != nil {
		log.Fatalf("ERROR: %s", err)
		return err
	}

	return nil
}

func createBlockKit() slack.MsgOption {
	// header section
	headerText := slack.NewTextBlockObject("mrkdwn", "セッションが終了しました。休憩時間を選択してください。", false, false)
	headerSection := slack.NewSectionBlock(headerText, nil, nil)

	// buttons
	shortBtnTxt := slack.NewTextBlockObject("plain_text", fmt.Sprintf("%s 分", os.Getenv("SHORT_REST_TIME_BUTTON_MINUTES")), false, false)
	shortBtn := slack.NewButtonBlockElement("", os.Getenv("SHORT_REST_TIME_BUTTON_MINUTES"), shortBtnTxt)

	longBtnTxt := slack.NewTextBlockObject("plain_text", fmt.Sprintf("%s 分", os.Getenv("LONG_REST_TIME_BUTTON_MINUTES")), false, false)
	longBtn := slack.NewButtonBlockElement("", os.Getenv("LONG_REST_TIME_BUTTON_MINUTES"), longBtnTxt)

	endBtnTxt := slack.NewTextBlockObject("plain_text", "作業中断", false, false)
	endBtn := slack.NewButtonBlockElement("", os.Getenv("WORK_END_KEYWORD"), endBtnTxt)

	actionBlock := slack.NewActionBlock("", shortBtn, longBtn, endBtn)

	return slack.MsgOptionBlocks(
		headerSection,
		actionBlock,
	)
}

func main() {
	lambda.Start(HandleRequest)
}

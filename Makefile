.PHONY: build clean deploy

build:
	dep ensure -v
	env GOOS=linux go build -ldflags="-s -w" -o bin/receiveEvent receiveEvent/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/receiveInteraction receiveInteraction/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/startTimer startTimer/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/askRestTime askRestTime/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/countRestTime countRestTime/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/endWork endWork/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/endRest endRest/main.go

clean:
	rm -rf ./bin ./vendor Gopkg.lock

deploy: clean build
	sls deploy --verbose

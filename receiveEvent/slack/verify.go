package slack

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/nlopes/slack"
	"github.com/nlopes/slack/slackevents"
)

// VerifyRequest : 評価
func VerifyRequest(body string, headers map[string]string) (slackevents.EventsAPIEvent, error) {

	eventsAPIEvent, err := slackevents.ParseEvent(
		json.RawMessage(body),
		slackevents.OptionVerifyToken(
			&slackevents.TokenComparator{
				VerificationToken: os.Getenv("SLACK_VERIFICATION_TOKEN"),
			},
		),
	)
	if err != nil {
		log.Printf("[ERROR]Failed to parse the passed event. : %s", err)
		return *new(slackevents.EventsAPIEvent), err
	}

	log.Printf("EventType: %s", eventsAPIEvent.Type)
	if eventsAPIEvent.Type == slackevents.URLVerification {
		return eventsAPIEvent, nil
	}

	if err := verifyHeaders(body, headers); err != nil {
		return *new(slackevents.EventsAPIEvent), err
	}

	return eventsAPIEvent, nil
}

func convertHeaders(headers map[string]string) http.Header {
	header := http.Header{}
	for key, value := range headers {
		header.Set(key, value)
	}
	return header
}

func verifyHeaders(body string, headers map[string]string) error {
	header := convertHeaders(headers)
	sv, err := slack.NewSecretsVerifier(header, os.Getenv("SLACK_SIGNING_SECRET"))
	if err != nil {
		return err
	}
	sv.Write([]byte(body))
	return sv.Ensure()
}

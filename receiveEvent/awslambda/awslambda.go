package awslambda

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"regexp"

	s "jp.co.linkode/PomodoroTecTimer/receiveEvent/slack"
	"jp.co.linkode/PomodoroTecTimer/shared/models"

	l "github.com/aws/aws-sdk-go/service/lambda"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/nlopes/slack/slackevents"
)

// HandleRequest is our lambda handler invoked by the `lambda.Start` function call
func HandleRequest(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	body := request.Body
	log.Printf("Request Body: %s", body)

	slackevent, verifyerr := s.VerifyRequest(body, request.Headers)

	if verifyerr != nil {
		return events.APIGatewayProxyResponse{
			Body:       request.Body,
			StatusCode: http.StatusInternalServerError,
		}, nil
	}

	log.Print(slackevent)
	if slackevent.Type == slackevents.URLVerification {
		return returnChallengeValue(request), nil

	} else if slackevent.Type == slackevents.CallbackEvent {
		svc := l.New(session.New())
		var input *l.InvokeInput

		innerEvent := slackevent.InnerEvent
		log.Printf("Inner Event: %s", innerEvent)
		switch event := innerEvent.Data.(type) {
		case *slackevents.AppMentionEvent:
			rep := regexp.MustCompile(`<@[A-Za-z0-9\.-_]*>\s`)
			text := rep.ReplaceAllString(event.Text, "")
			log.Printf("workDetail: %s", text)
			workDetail := models.WorkDetail{
				Content: text,
			}
			log.Printf("function name: %s", os.Getenv("ARN_START_TIMER"))
			payload, _ := json.Marshal(workDetail)
			input = &l.InvokeInput{
				FunctionName:   aws.String(os.Getenv("ARN_START_TIMER")),
				Payload:        payload,
				InvocationType: aws.String("Event"),
			}
			resp, err := svc.Invoke(input)

			if err != nil {
				log.Printf("[ERROR]Failed to invove function : %s", err)
				return events.APIGatewayProxyResponse{
					Body:       request.Body,
					StatusCode: http.StatusInternalServerError,
				}, nil
			}
			log.Printf("Invoke function result : %s", resp)

		default:
			log.Print("ERROR: An unexpected event was passed.")
			return events.APIGatewayProxyResponse{
				Body:       request.Body,
				StatusCode: http.StatusInternalServerError,
			}, nil
		}
	}

	return events.APIGatewayProxyResponse{
		Body: "OK",
		Headers: map[string]string{
			"Content-Type": "text"},
		StatusCode: 200,
	}, nil
}

func returnChallengeValue(request events.APIGatewayProxyRequest) events.APIGatewayProxyResponse {
	var r *slackevents.ChallengeResponse

	if err := json.Unmarshal([]byte(request.Body), &r); err != nil {
		log.Printf("[ERROR]Failed to parse URL Verification Event. : %s", err)
		return events.APIGatewayProxyResponse{
			Body:       request.Body,
			StatusCode: http.StatusInternalServerError,
		}
	}

	return events.APIGatewayProxyResponse{
		Body: r.Challenge,
		Headers: map[string]string{
			"Content-Type": "text"},
		StatusCode: 200,
	}
}

// StartLambda : Lambda のハンドラを起動する
func StartLambda() {
	lambda.Start(HandleRequest)
}
